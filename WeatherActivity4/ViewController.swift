//
//  ViewController.swift
//  WeatherActivity4
//
//  Created by Jyothis Rajan on 2019-11-05.
//  Copyright © 2019 Jyothis Rajan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity
import Particle_SDK

class ViewController: UIViewController, WCSessionDelegate{
    @IBOutlet weak var timeBtn: UIButton!
    
    @IBOutlet weak var todayTempBtn: UIButton!
    @IBOutlet weak var rainBtn: UIButton!
    
    var API_KEY = "ef20f6ff0148153e4436c2db3c05213e"
    var cityName:String = "Toronto"
    var timeZone:String = "America/Toronto"
    var currentTime = ""
    
    // MARK: User variables
    let USERNAME = "jyothisrajant@gmail.com"
    let PASSWORD = "jyrinternet"
    
    // MARK: Device
    // Jenelle's device
    //let DEVICE_ID = "36001b001047363333343437"
    // Antonio's device
    let DEVICE_ID = "1c002a001247363333343437"
    var myPhoton : ParticleDevice?
    
    // MARK: Other variables
    var gameScore:Int = 0
    var hour=""
    var minute=""
    var temperature=""
    var humidity=""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        } // end login
        if WCSession.isSupported() {
            print("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            print("\nPhone does not support WCSession")
        }
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                
            }
            
        } // end getDevice()
    }
    
  
    
    func getTimeData(){
        print("Called world time  API")
        let URL = "https://worldtimeapi.org/api/\(self.timeZone)/"
        //print(URL)
        Alamofire.request(URL).responseJSON {
            (currentTimeReceived) in
            print(currentTimeReceived)
            let responseTime = JSON(currentTimeReceived.value)
            let dateTime = responseTime["datetime"]
            print("Date time received \(dateTime)")
            self.getHourTime(time: dateTime.string!)
            self.getMinTime(time: dateTime.string!)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
            self.cityName =  (message["cityname"] as? String)!
            self.timeZone =  (message["timezone"] as? String)!
            if self.cityName != ""{
                self.getWeatherData()
            }
            if self.timeZone != "" {
                self.getTimeData()
            }
            
        }
    }
    
    func getHourTime(time: String){
        var time = time.components(separatedBy: "T")
        var timeData = time[1].components(separatedBy: ".")
        var hourData = timeData[0].components(separatedBy: ":")
        hour = hourData[0]
        print("The hour data is \(hour)")
    }
    func getMinTime(time: String){
        var time = time.components(separatedBy: "T")
        var timeData = time[1].components(separatedBy: ".")
        var MinData = timeData[0].components(separatedBy: ":")
        minute = MinData[1]
        print("The minute data is \(minute)")
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func sendHourToParticle() {
        
        print("Pressed the hour button,hour="+hour)
        
        let parameters = [""+hour]
        print("hour="+hour)
        var task = myPhoton!.callFunction("sethour", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle hour")
            }
            else {
                print("Error when telling Particle hour")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    func sendMinToParticle() {
        
        print("Pressed the min button,min="+minute)
        
        let parameters = [""+minute]
        print("min="+minute)
        var task = myPhoton!.callFunction("setmin", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle min")
            }
            else {
                print("Error when telling Particle min")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    func sendTempToParticle() {
        
        print("Pressed the temp button,temperature="+temperature)
        //let tempC=Int(temperature)!-273
        let parameters = [""+temperature]
        print("temperature in K="+temperature)
        // print("temperature in C="+String(tempC))
        var task = myPhoton!.callFunction("settemp", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle temp")
            }
            else {
                print("Error when telling Particle temp")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    func sendRainToParticle() {
        
        //let tempC=Int(temperature)!-273
        let parameters = [""+humidity]
        print("humidity="+humidity)
        // print("temperature in C="+String(tempC))
        var task = myPhoton!.callFunction("setrain", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle temp")
            }
            else {
                print("Error when telling Particle temp")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    @IBAction func timeBtnAction(_ sender: Any) {
        getTimeData()
        sendHourToParticle()
        sendMinToParticle()
    }
    
    @IBAction func todayTempBtnAction(_ sender: Any) {
        getWeatherData()
        sendTempToParticle()
    }
    
    @IBAction func rainBtnAction(_ sender: Any) {
        
          getWeatherData()
        sendRainToParticle()
        
    }
    func getWeatherData() {
        print("Called weather data API")
        let URL = "https://api.openweathermap.org/data/2.5/weather?q=\(self.cityName)&appid=\(API_KEY)"
        //print(URL)
        Alamofire.request(URL).responseJSON {
            (currentWeather) in
            print(currentWeather.value)
            
            // convert the response to a JSON object
            
            let jsonResponse = JSON(currentWeather.value)
            let main = jsonResponse["name"]
            let description = jsonResponse["weather"]
            let temp = jsonResponse["main"]["temp"]
            let desc = description[0]["description"]
            
            let mainJSON = jsonResponse["main"]["temp"]
            let humidity=jsonResponse["main"]["humidity"]
            self.temperature=temp.description
            self.humidity=humidity.description
            print(self.temperature)
            
        }
    }
    
    
}



