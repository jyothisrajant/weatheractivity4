// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton b = InternetButton();

void setup() {

 b.begin();
    Particle.function("sethour",setHour);
    Particle.function("setmin",setMin);
    Particle.function("settemp",setTemp);
    Particle.function("setrain",setRain);
}

void loop() {

}

int setHour(String cmd){
  
    int hour = cmd.toInt();
    if(hour>12){
        hour=hour-12;
    }
   
    if(hour==12){
        
       
        
    }else{
        b.ledOn(hour, 0, 0, 255); 
    }
     delay(3000);
     b.allLedsOff();
}
int setMin(String cmd){
   
    int min = (cmd.toInt())/5;
    b.ledOn(min, 255, 0, 0); 
    delay(3000);
     b.allLedsOff();
}

int setTemp(String cmd){
   
    float temp = (cmd.toInt());
    if(temp>303){
        for(int i=1;i<=11; i++){{
           b.ledOn(i,255, 0, 0);
           delay(500);
        }}
    }else if(temp>293 && temp<303){
        for(int i=1;i<=8; i++){{
           b.ledOn(i,0, 255, 0); 
           delay(500);
        }}
        
    }else if(temp>273 && temp<293){
        for(int i=1;i<=6; i++){{
           b.ledOn(i,0, 0, 255); 
           delay(500);
        }}
       
    }else if(temp<273){
        for(int i=1;i<=3; i++){{
           b.ledOn(i,255, 255, 255); 
           delay(500);
        }}
    }
    delay(5000);
     b.allLedsOff();
}
int setRain(String cmd){
   b.allLedsOff();
    int rain=cmd.toInt();
    int calc=rain/10;
    for(int i=1;i<=calc; i++){
           b.ledOn(i,255, 255, 255);
           delay(500);}
}